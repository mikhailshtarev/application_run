package com.example.demo.logic;

import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Scanner;

/**
 * Логика для регистрации
 */

@Service
public class RegistrationImpl implements Registration {

    @Override
    public String getFirstName(@NonNull final Scanner scanner) {
        String firstName = "";
        boolean condition = true;
        while (condition) {
            boolean noNumber = true;
            System.out.println("Введите имя");
            firstName = scanner.nextLine();
            for (char myChar : firstName.toCharArray()) {
                if (Character.isDigit(myChar)) {
                    noNumber = false;
                }
            }
            if ("".equals(firstName.strip()) || firstName.strip().isEmpty() || firstName.strip().length() < 2) {
                System.err.println("Имя введено некорректно! Длинна имени должна быть более 3-х символов.");
                condition = true;
            } else if (!noNumber) {
                System.err.println("Имя не должно содержать цифр");
                condition = true;
            } else condition = false;
        }
        return firstName;
    }

    @Override
    public String getSecondName(@NonNull final Scanner scanner) {
        String secondName = "";
        boolean condition = true;
        while (condition) {
            boolean noNumber = true;
            System.out.println("Введите Фимилию");
            secondName = scanner.nextLine();
            for (char myChar : secondName.toCharArray()) {
                if (Character.isDigit(myChar)) {
                    noNumber = false;
                }
            }
            if ("".equals(secondName.strip()) || secondName.strip().isEmpty() || secondName.strip().length() < 2) {
                System.err.println("Фамилия введена некорректно");
                condition = true;
            } else if (!noNumber) {
                System.err.println("Фамилия не должна содержать цифр");
                condition = true;
            } else condition = false;
        }
        return secondName;
    }
}
