package com.example.demo.logic;

import com.example.demo.entity.AskParentEntity;

import java.util.List;
import java.util.Scanner;

public interface Opros {
    List<String> getResult(AskParentEntity askParentEntity, Scanner scanner);
}
