package com.example.demo.logic;

import com.example.demo.entity.AskParentEntity;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Логика для проведения опроса
 */

@Service
public class OprosImpl implements Opros {

    @Override
    public List<String> getResult(@NonNull final AskParentEntity askParentEntity, @NonNull final Scanner scanner) {

        if (askParentEntity.getAskEntityList().isEmpty()) {
            throw new NullPointerException();
        }

        ArrayList<String> listAnswer = new ArrayList<>();
        List<Integer> list = new ArrayList<>();

        askParentEntity.getAskEntityList().forEach(askEntity -> {
            boolean condition = true;
            while (condition) {
                try {
                    System.out.println(askEntity.getAsk());
                    System.out.println(Arrays.toString(askEntity.getOptionAnswer())
                            .replace("[", "").replace("]", ""));
                    final String consoleAnswer = scanner.nextLine();
                    if (Integer.parseInt(consoleAnswer) > askEntity.getOptionAnswer().length || Integer.parseInt(consoleAnswer) < 1) {
                        System.err.println("Ответ должен быть в диапазоне от 1 до " + askEntity.getOptionAnswer().length);
                        condition = true;
                    } else {
                        listAnswer.add(askEntity.getAsk() + "Ответ: " + consoleAnswer + "  ");
                        list.add(Integer.valueOf(consoleAnswer));
                        condition = false;
                    }
                } catch (Exception e) {
                    System.err.println("Ошибка, Ответ должен быть числом: ");
                    condition = true;
                }

            }
        });

        listAnswer.add("Сумма баллов: " + list.stream().mapToInt((value) -> Integer.parseInt(String.valueOf(value))).sum());
        return listAnswer;
    }
}
