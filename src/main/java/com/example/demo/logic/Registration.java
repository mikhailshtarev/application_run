package com.example.demo.logic;

import java.util.Scanner;

public interface Registration {
    String getFirstName(Scanner scaner);

    String getSecondName(Scanner scaner);
}
