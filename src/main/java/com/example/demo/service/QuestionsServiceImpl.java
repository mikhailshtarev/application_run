package com.example.demo.service;

import com.example.demo.entity.AskParentEntity;
import com.example.demo.entity.ResultEntity;
import com.example.demo.logic.Opros;
import com.example.demo.logic.Registration;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Scanner;

/**
 * Общий сервис приложения (основная логика)
 */

@Service
public class QuestionsServiceImpl implements QuestionsService {

    private final Registration registration;
    private final Opros opros;
    private final Scanner scanner = new Scanner(System.in);
    private final AskParentEntity askParentEntity;

    @Autowired
    public QuestionsServiceImpl(@NonNull final Registration registration,
                                @NonNull final Opros opros,
                                @NonNull final AskParentEntity askParentEntity) {
        this.registration = registration;
        this.opros = opros;
        this.askParentEntity = askParentEntity;
    }


    public ResultEntity startQuestions() {
        ResultEntity resultEntity;
        resultEntity = new ResultEntity(
                registration.getFirstName(scanner),
                registration.getSecondName(scanner),
                opros.getResult(askParentEntity, scanner));
        System.out.println(resultEntity);
        return resultEntity;
    }
}
