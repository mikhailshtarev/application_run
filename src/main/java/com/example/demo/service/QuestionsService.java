package com.example.demo.service;

import com.example.demo.entity.ResultEntity;

public interface QuestionsService {
    ResultEntity startQuestions();
}
