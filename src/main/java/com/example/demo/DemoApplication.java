package com.example.demo;

import com.example.demo.service.QuestionsService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
        QuestionsService questionsService = ctx.getBean(QuestionsService.class);
        Thread thread = new Thread();
        synchronized (thread) {
            thread.wait(1000);
        }
        questionsService.startQuestions();

    }

}
