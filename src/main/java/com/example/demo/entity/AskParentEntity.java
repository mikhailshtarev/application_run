package com.example.demo.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Entity для парсинга Вопросов из application.yml
 */

@Component
@ConfigurationProperties("application")
public class AskParentEntity {
    private List<AskEntity> askEntityList;

    public List<AskEntity> getAskEntityList() {
        return askEntityList;
    }

    public void setAskEntityList(List<AskEntity> askEntityList) {
        this.askEntityList = askEntityList;
    }
}
