package com.example.demo.entity;

import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Entity для вывода результата
 */

@Data
public class ResultEntity {
    /**
     * Имя студента
     */
    private String FirstName;
    /**
     * Фамилия студента
     */
    private String SecondName;
    /**
     * Результаты опроса
     */
    private List<String> arrayListResult;

    public ResultEntity(String firstName, String secondName, List<String> arrayListResult) {
        FirstName = firstName;
        SecondName = secondName;
        this.arrayListResult = arrayListResult;
    }

    @Override
    public String toString() {
        return "Результаты теста: " + '\n'+
                "Имя студента : " + FirstName + '\n'+
                "Фамилия студента : " + SecondName + '\n' +
                "Результаты студента: " + arrayListResult.stream().map(x->x+'\n').collect(Collectors.joining());
    }
}
