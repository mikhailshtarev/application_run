package com.example.demo.entity;


import lombok.Getter;
import lombok.Setter;

/**
 * Entity одного вопроса
 */

@Getter
@Setter
public class AskEntity {
    /**
     * Вопрос
     */
    private String ask;
    /**
     * Варианты ответа
     */
    private String[] optionAnswer;
}
