package com.example.demo;

import lombok.NonNull;

import java.util.Random;

public class TestRandom {
    public static int getRamdom(@NonNull final int min,@NonNull final int max){
        Random random = new Random();
        return random.ints(min, max).findAny().getAsInt();
    }
}
