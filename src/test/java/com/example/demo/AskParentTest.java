package com.example.demo;

import com.example.demo.entity.AskParentEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
public class AskParentTest {
    @Autowired
    private AskParentEntity askParentEntity=new AskParentEntity();

    @Test
    public void askParentTest() {
        assertEquals("Укажите Ваш курс.", askParentEntity.getAskEntityList().get(0).getAsk());
        assertEquals(5, askParentEntity.getAskEntityList().size());
        assertNotNull(askParentEntity.getAskEntityList().get(TestRandom.getRamdom
                (0, askParentEntity.getAskEntityList().size() - 1)));
    }

}
