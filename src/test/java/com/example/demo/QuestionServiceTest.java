package com.example.demo;

import com.example.demo.entity.ResultEntity;
import com.example.demo.logic.Opros;
import com.example.demo.logic.Registration;
import com.example.demo.service.QuestionsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class QuestionServiceTest {

    @Autowired
    private QuestionsServiceImpl questionsService;
    @MockBean
    private Registration registration;
    @MockBean
    private Opros opros;

    @Test
    public void startQuestions() {
        List<String> oprosList = new ArrayList<>();
        oprosList.add("ask-1");
        oprosList.add("ask-2");
        when(this.registration.getFirstName(any())).thenReturn("FirstName");
        when(this.registration.getSecondName(any())).thenReturn("SecondName");
        when(this.opros.getResult(any(), any())).thenReturn(oprosList);
        ResultEntity resultEntity = new ResultEntity("FirstName", "SecondName", oprosList);

        assertEquals(resultEntity, questionsService.startQuestions());
    }
}
